#undef UNICODE

#include <stdlib.h>
#include <stdio.h>
#include "SCSingleton.h"
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	 
 
	switch(argc)
	{
	case 1:
		cout << "*****Multiplayer Server-Client System*****\n\n";
		cout << "\n\n **** SERVER MODE ****\n";
		
		SCSingleton::Instance()->CreateServerInstance();

		

		break;

	case 2:
		cout << "*****Multiplayer Server-Client System*****\n\n";
		cout << "\n\n **** CLIENT MODE (Client Name: " << argv[1] <<") ****\n\n";

		SCSingleton::Instance()->CreateClientInstance(argv[1]);

		break;

	default:
		cout << "\n*****Multiplayer Server-Client System*****\n\n";
		cout << "***INSTRUCTIONS***"<<endl;
		cout << " TO LAUNCH AS SERVER:: Execute this program with no arguments." << endl;
		cout << " TO LAUNCH AS A CLIENT:: Execute this program with one argument: clientname"<<endl;

		system("pause");
		return 0;
	}//end if

//	cout << "*****Multiplayer Server-Client System*****\n\n";



		system("pause");
		return 0;
}