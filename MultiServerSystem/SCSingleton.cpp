#define WIN32_LEAN_AND_MEAN
#ifndef WIN32_LEAN_AND_MEAN
#endif // !WIN32_LEAN_AND_MEAN


//#include <stddef.h>  // defines NULL
#include "SCSingleton.h"
#include <Windows.h>
#include <WinSock2.h>
#include <stdio.h>
#include <wS2tcpip.h>
#include <stdlib.h>
#include <process.h>
 


ClientInfo::ClientInfo(string cn,SOCKET cs)
{
	clientName = cn; clientSocket = cs;
	isLoggedOff = false;
}

static int ConvertCharToInt(char c)
{
	return (int)c - 48;
}



unsigned __stdcall ClientSession(void *data)
{

	char recvBuf[DEFAULT_BUFLEN];
	int recvBufLength = DEFAULT_BUFLEN;
	int iResult;
	int iSendResult;
	SOCKET ClientSocket = (SOCKET)data;

	iResult = recv(ClientSocket, recvBuf, recvBufLength, 0);
	if (iResult > 0)
	{
		std::cout << "***(" << recvBuf << ") has joined the chat...**** " << endl;
		string _str; 
		string _cn;
		
		for (int i = 0; i < iResult; i++)
		{
			_str += recvBuf[i]; _cn += recvBuf[i];
		}

		_str += ",join";
		 
		char buf[] = "";

		strcpy(buf,_str.c_str());

		SCSingleton::Instance()->RegisterClient(_cn, ClientSocket);

		SCSingleton::Instance()->PropagateMessageToOtherClients(_cn, buf, iResult+6,false);

	}

	do
	{
		std::cout << "\nMessaging System Server:: Waiting for a client to send a message....(type quit to shutdown server) \n\n";

	

		iResult = recv(ClientSocket, recvBuf, recvBufLength, 0);

		string clientName, msg;
		if (iResult > 0) {
			/****** MESSAGE INTERCEPTION *******/
			 
			int firstCommaIndex;

			for (int i = 0; i < iResult; i++)
			{
				if (recvBuf[i] == ',')
					firstCommaIndex = i;
			}

			std::string buf(recvBuf);

			 clientName = buf.substr(0, firstCommaIndex);
			msg = buf.substr(firstCommaIndex + 1, iResult - firstCommaIndex - 1);

			if (msg == "logoff")
			{
				std::cout << clientName << " logged off... \n";
				//propagate before logging off
				SCSingleton::Instance()->PropagateMessageToOtherClients(clientName, recvBuf, iResult,true);


				break;
			}
			else
			{
				cout << "(" << clientName << ")::" << msg << endl;
				printf("\n");

				//Check queue....
				SCSingleton::Instance()->PropagateMessageToOtherClients(clientName, recvBuf, iResult,false);


			}


		 
		}

		else if (iResult == 0)
		{
			printf("Messaging Server System: Closing connection with client...   \n");
		}
		else
		{
			if(!SCSingleton::Instance()->isLoggedOffServerPending)
			printf("Messaging Server System: Recv Failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			system("pause");
			return 1;
		}

	


	 
	} while (iResult > 0);

	iResult = shutdown(ClientSocket, SD_SEND);

	if (iResult == SOCKET_ERROR) {
		printf("Messaging Server System: Shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		system("pause");
		return 1;
	}

	closesocket(ClientSocket);
}

// Global static pointer used to ensure a single instance of the class.
SCSingleton* SCSingleton::m_pInstance = NULL;

/** This function is called to create an instance of the class.
Calling the constructor publicly is not allowed. The constructor
is private and is only called by this Instance function.
*/

SCSingleton* SCSingleton::Instance()
{
	if (!m_pInstance)   // Only allow one instance of class to be generated.
	{
		m_pInstance = new SCSingleton;
	 
	}


	return m_pInstance;
}


bool SCSingleton::IsClientExists(string clientName)
{
	for (int i = 0; i < clients.size(); i++)
	{
		if (clients[i].clientName == clientName)
			return true;
	}
	return false;
}
void SCSingleton::RegisterClient(string clientName,SOCKET cs)
{
	
	std::cout << "received request to register user: " << clientName<<endl;

	// new registration...add to list
	if (!IsClientExists(clientName))
	{
		// It does not point to end, it means element exists in list
		std::cout << "'client not found in registration list... adding to list:: "<<clientName << std::endl;
	
		ClientInfo ci(clientName,cs);
		clients.push_back(ci);
	}

}

int SCSingleton::CreateServerInstance()
{


	WSADATA wsaData;

	int iResult;
	SOCKET ClientSocket = INVALID_SOCKET;


	struct addrinfo *result = NULL, hints;
	ZeroMemory(&hints, sizeof(hints));

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

	//create the winsocket
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n, iResult");

		return 1;

	}


	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);

	if (iResult != 0) {
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
		return 1;
	}

     ListenSocket = INVALID_SOCKET;

	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (ListenSocket == INVALID_SOCKET) {
		printf("Messaging Server System:: Error encounted at Socket: %d\n", WSAGetLastError());

		//printf("error at socket(): %d\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		system("pause");

		return 1;
	}

	iResult = bind(ListenSocket, result->ai_addr, int(result->ai_addrlen));

	if (iResult == SOCKET_ERROR) {
		printf("Messaging Server System:: Bind Failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		system("pause");

		return 1;
	}

	freeaddrinfo(result);

	if (listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR) {
		printf("Messaging Server System:: Listen Failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		system("pause");

		return 1;
	}
	std::cout << "***MESSAGING SYSTEM SERVER***\n\n";
	std::cout << ("Waiting for a client to connect....\n");

	SCSingleton::Instance()->CreateUserInputServerThread();


	while (!isLoggedOffServerPending &&  (ClientSocket = accept(ListenSocket, NULL, NULL))) {
		//std::cout << "Firing up...\n";

		if (ClientSocket == INVALID_SOCKET) {
			if (isLoggedOffServerPending)
			{
				cout << "Server quitting...";
			}
			else
			{
				printf("Messaging System Server:: Accept failed (error: %d)\n", WSAGetLastError());
			}

			closesocket(ListenSocket);
			WSACleanup();
			system("pause");

			return 1;
		}

		// Create a new thread for the accepted client (also pass the accepted client socket).
		unsigned threadID;
		HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &ClientSession, (void*)ClientSocket, 0, &threadID);


	}

	//if (isLoggedOffServerPending)
		//cout << "Server shutdown was requested...";

	WSACleanup();
	std::cout << "****Shutting down messaging system server ****" << std::endl;
	return 0;
}
 
 /*
void SCSingleton::AddMessageToQueue(string clientName, string msg)
{
	for (int i = 0; i < clients.size(); i++)
	{
		if (clients[i].clientName != clientName)
		{
			ClientMessagePacker cmp(clientName, msg);
			clients[i].msgListToPush.push_back(cmp);
		}
	}

}*/

//void ClientInfo::RemoveMessages()
//{
//	msgListToPush.clear();
//}

int SCSingleton::CreateClientInstance(string clientName)
{
	WSADATA wsaData;
	ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("Unable to connect to the messaging system server. WSAStartup failed with error: %d\n", iResult);
		system("pause");
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo("localhost", DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("Unable to connect to the messaging system server. getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("Unable to connect to the messaging system server. Socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			system("pause");
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	std::cout << "****CLIENT MESSAGING SYSTEM STATUS: ACTIVE****\n";
	std::cout << "****CLIENT NAME: "<<clientName <<"****\n\n";

	std::cout << "Instructions: Type and press enter to send to server. Type logoff to close the connection. Note: The connection will automatically close if thet server quits\n\n";
	
	string msg;
	char msgBuf[] = "";

	char clientNameChar[] = "";
	strcpy(clientNameChar, clientName.c_str());
	iResult = send(ConnectSocket, clientNameChar, strlen(clientNameChar), 0); //send client name

	CreateUserInputClientThread();
	CreateMessageListenerThread();
	//loop until exit is done
	do {
	

 		if (isInputReceived&&!isLoggedOffCurrentUser&&!isServerShutdownMsgReceived)
		{
			cout << "You:: " << input << endl;
			//cout << "Processing...input = "<<input << endl;
			msg = clientName + "," + input;

			strcpy(msgBuf, msg.c_str());

			int sendbufLen = (int)strlen(msgBuf);

			iResult = send(ConnectSocket, msgBuf, sendbufLen, 0);
			if (iResult == SOCKET_ERROR) {
				printf("Send failed with error: %d\n: ", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				system("pause");
				return 1;
			}//end if
			 //	printf("Bytes sent: %ld\n", iResult);

		 

			isInputReceived = false;
			CreateUserInputClientThread();
		}

		if (isLoggedOffCurrentUser||isServerShutdownMsgReceived)
			break;
	 


	} while (iResult > 0);

	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		system("pause");
		return 1;
	}//end if


	 // cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	std::cout << "*****Exiting Client******";
	system("pause");
	return 0;
}

int _iResult;
char _recvbuf[DEFAULT_BUFLEN];
int _recvbuflen = DEFAULT_BUFLEN;

void CheckForIncomingMessages(void* data)
{

	do {
		int _iResult = recv(SCSingleton::Instance()->ConnectSocket, _recvbuf, _recvbuflen, 0);
		if (_iResult > 0) {
			//if (iResult != sendbufLen)//verify whether the sent message length is the same as the received message length. if it isn't then the message is likely corrupted
			//printf("Failed check.. received message is different than sent one. received length: %d, sent length: %d\n", iResult, sendbufLen);
			//	else 
		//	cout << "\nReceived message from server: " << _recvbuf << "...\n";
			SCSingleton::Instance()->ParseIncomingMessage(_recvbuf, _iResult);
			//SCSingleton::Instance()->isIncomingMessageReceived = true;
		}
	} while (true);

 }

 void WaitForUserInput(void* data)
{
	do
			{
		if (SCSingleton::Instance()->isLoggedOffCurrentUserPending)
		{
			SCSingleton::Instance()->isLoggedOffCurrentUser = true;
			break;
		}
				//std::cout << "\nEnter a message to send to server: ";
				 
				std::getline(std::cin, SCSingleton::Instance()->input);
				//std::cout << "user typed: " << SCSingleton::Instance()->input << endl;
				SCSingleton::Instance()->isInputReceived = true;

				if (SCSingleton::Instance()->input == "logoff")
				{
					SCSingleton::Instance()->isLoggedOffCurrentUserPending = true;
				
				}
				
			} 
			while (SCSingleton::Instance()->iResult > 0 && !SCSingleton::Instance()->isInputReceived);
		
			//return 0;

}

void SCSingleton::CreateUserInputClientThread()
{
	unsigned threadID;
	int data = 0;
	HANDLE  myhandleC = (HANDLE)_beginthread(&WaitForUserInput, 0, 0);
}

void SCSingleton::CreateMessageListenerThread()
{
	unsigned threadID;
	int data = 0;
	HANDLE  myhandleC = (HANDLE)_beginthread(&CheckForIncomingMessages, 0, 0);
}

void WaitForUserInputServer(void* data)
{
	do
	{
		string inp;

		std::getline(std::cin,inp );

		if (inp== "quit")
		{
			SCSingleton::Instance()->isLoggedOffServerPending = true;
			cout << "quit requested....\n";
			SCSingleton::Instance()->PropagateShutdownMsgToClients();

			closesocket(SCSingleton::Instance()->ListenSocket);
			WSACleanup();
			system("pause");
			 

		}

	} while (SCSingleton::Instance()->iResult > 0 && !SCSingleton::Instance()->isLoggedOffServerPending);



}


void SCSingleton::CreateUserInputServerThread()
{
	unsigned threadID;
	int data = 0;
	HANDLE  myhandleC = (HANDLE)_beginthread(&WaitForUserInputServer, 0, 0);
}

void SCSingleton::PropagateMessageToOtherClients(string cn,char msg[512], int msgLen,bool isLogOff)
{


	for (int i = 0; i < clients.size(); i++)
	{
		if (clients[i].clientName != cn)
		{
			if (clients[i].isLoggedOff) continue;
			cout << "Propagating to: " << clients[i].clientName<<endl;
			int iSendResult = send(clients[i].clientSocket, msg, msgLen, 0);
			
		}
		else
		{
			if (isLogOff)
				clients[i].isLoggedOff = true;
		}
	}
}


void SCSingleton::PropagateShutdownMsgToClients()
{

	if (!isLoggedOffServerPending) return;
	char buf[16] = "server,servdown";

	for (int i = 0; i < clients.size(); i++)
	{
		if (clients[i].isLoggedOff) continue;
			int iSendResult = send(clients[i].clientSocket, buf, 16, 0);
		 
	}

}

void SCSingleton::ParseIncomingMessage(char recvBuf[512],int iResult)
{
//	cout << "**:: " << recvBuf << "length: "<<iResult<<endl;
	int __firstCommaIndex;

	for (int i = 0; i < iResult; i++)
	{
		if (recvBuf[i] == ',')
			__firstCommaIndex = i;
	}

	std::string __buf(recvBuf);

	string __clientName = __buf.substr(0, __firstCommaIndex);
	string __msg = __buf.substr(__firstCommaIndex + 1, iResult - __firstCommaIndex - 1);

	if (__msg == "logoff")
	{
		cout <<"\n(" <<__clientName<<") has logged off...\n";
	}
	else if (__msg == "join")
	{
		cout << "\n****(" << __clientName << ") has just joined...****\n";
	}
	else if (__msg == "servdown")
	{
		cout << "\n****(Server has been shut down)****\n";
		isServerShutdownMsgReceived = true;
	}
	else
	{
		cout << "\n(" << __clientName << "):: "<<__msg<<endl;
	}

}