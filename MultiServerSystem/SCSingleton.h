#pragma once

#ifndef __SCSINGLETON_INCLUDED__
#define __SCSINGLETON_INCLUDED__

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <WinSock2.h>
#include <wS2tcpip.h>

using namespace std;


#pragma comment (lib, "Ws2_32.lib")
#define DEFAULT_PORT "4816"
#define DEFAULT_BUFLEN 512


class ClientInfo
{
public:
	string clientName;
	SOCKET clientSocket;
	ClientInfo(string,SOCKET);
	bool isLoggedOff;
};

class SCSingleton {

public:
	static SCSingleton* Instance();
 	int CreateServerInstance();
	int CreateClientInstance(string);
	void RegisterClient(string, SOCKET);

	void AddMessageToQueue(string, string);
	
	void PropagateMessageToOtherClients(string,char n[512],int,bool);

	std::vector<ClientInfo> clients;
//	int totalClients;
	string input;
	bool isInputReceived;
	//bool isIncomingMessageReceived;
	int iResult;
	bool isInterrupted = false;

	SOCKET ConnectSocket;
	void ParseIncomingMessage(char recvBuf[512], int);
	bool isLoggedOffCurrentUser = false;
	bool isLoggedOffCurrentUserPending = false;
	bool isLoggedOffServerPending = false;
	SOCKET ListenSocket;
	void PropagateShutdownMsgToClients();
	bool isServerShutdownMsgReceived = false;
private:
	void CreateUserInputClientThread();
	void CreateUserInputServerThread();

	void CreateMessageListenerThread();

	SCSingleton() {};  // Private so that it can  not be called
	SCSingleton(SCSingleton const&) {};             // copy constructor is private
	SCSingleton& operator=(SCSingleton const&) {};  // assignment operator is private
	static SCSingleton* m_pInstance;

	bool IsClientExists(string);
 
	std::string checkForExitCommand = "logoff";
 

	

};


#endif // !__SCSINGLETON_INCLUDED__
